#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# :Project:   snips-test --
# :Created:   sab 09 feb 2019 15:57:25 CET
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2019 Alberto Berti
#

from hermes_python.hermes import Hermes


def intent_received(hermes, intent_message):
    print('Intent {}'.format(intent_message.intent))

    for (slot_value, slot) in intent_message.slots.items():
        print('Slot {} -> \n\tRaw: {} \tValue: {}'.format(slot_value, slot[0].raw_value, slot[0].slot_value.value.value))

    hermes.publish_end_session(intent_message.session_id, None)


def calc_somma(hermes, msg):
    hermes.publish_end_session(msg.session_id, "Hi Aries, you are a genius!")


with Hermes('snips.local:1883') as h:
    #h.subscribe_intents(intent_received)
    h.subscribe_intent("calcolaSomma", calc_somma)
    h.start()
